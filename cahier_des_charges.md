# Cahier des charges

## Cargo

### Projet Full-Stack 2022 Année 2021-2022

---

## 1. Contexte du projet

Fin 2020, Luc Vehi détecte que le trajet pour accéderaux centres de formation est parfois complexe et nécessite une organisation importante de la part des apprenants. Il a alors l’idée de créer une application de covoiturage dédiée aux écoles supérieures.

---

## Objectif

En tant qu’agence de développement informatique, nous avons pour mission,
en appliquant les bonnes pratiques de gestions de projets agiles, de :

- Comprendre le besoin client
- Créer un cahier des charges
- Concevoir l’application
- Maquetter l’application
- Développer une solution et la mettre en production

## Acteurs

##### Client

Luc Véhi, sérial-entrepreneur, est directeur d’un centre de formation.
Pour les besoins en transport de ses étudiants, il a besoin d’une application qui coordonnera les covoiturages.

---

### Prestataire

Alexis Ducoulombier et Emmanuel Bosquet, étudiants à l’école 404, relèvent le défi.
Fort de leurs compétences en back-end et front-end, ils ont à cœur de relever les défis de la conception, du design, de l’intégration, du développement, de la production et de la maintenance.

---

### Cibles

Les principales cibles de cette application sont les étudiants,
les formateurs, et toutes autres personne travaillant sur les campus,
centre de formation ou université. A ceux qui souhaitent faire du covoiturage pour rendre le trajet plus pratique,
jovial et qui apporte un plus faible impact sur l'environnement.

![img](img/persona_1.png)
![img](img/persona_2.png)

---

### User Stories

Les utilisateurs, après s’être enregistrés et loggés, peuvent :

- consulter et nodifier leur profil
- être soit conducteur, soit passager.
- se déconnecter

En tant que conducteur, ils pourront :

- enregistrer leur voiture sur leur compte
- créer un covoiturage avec un point de départ, un point d’arrivée, des horaires, la voiture, le nombre de place disponibles, le prix proposé
- éditer leur covoiturage
- accepter ou refuser un passager
- valider un covoiturage
- consulter le profil d’un candidat passager

Un passager peut :

- consulter tous les covoiturages disponibles en fonction de filtres (point de départ, d’arrivée, horaires, voitures, etc)
- choisir un covoiturage
- consulter les covoiturages auquel il participe
- consulter le profil du conducteur
- se désinscrire d’un covoiturage

![img](img/use_cases_with_lines.jpg)

-------

## 2. Conception

## Charte graphique

### Nom

Pourquoi **Cargo** ? Nous avons utilisé le nom **Cargo** car "Car" = voiture, "Go" = aller vers.

### Logo

![img](img/logo.png)

Nous avont utilisé ce logo avec :

- la lettre "C" qui correspond à la premiere lettre de *Cargo*,
- le point qui a pour but de representez le "pin" lorsque l'on indique un endroit sur une carte.

### Couleurs

![img](img/palette_de_couleur.png)

Nous allons utiliser le vert pour rappeler l'environement.

### Polices

Nous allons utiliser la police **Roboto** pour sa simplicté et son utilisation en web.

### Mockup

![img](img/mockup.png)

voici un premier rendu des couleurs et de la police dans un cas d'une application mobile.

---

### Diagrammes

#### Diagramme de base de donnée

![img](img/class_diagram_with_aggregation.jpg)

---

### Technologies

#### PostgresQL

PostgresQL est un système de gestion de base de données relationnel.
Open source, fiable, présentant de nombreux progrès face à MySQL, il est un choix judicieux.

![img](https://devopstales.github.io/img/postgres.png)

#### Laravel + OpenAPI

Laravel est un framework PHP open source largement utilisé pour construire des APIs.

![img](https://www.secret-source.eu/wp-content/uploads/2017/11/Laravel-logo-300x300.jpg)

OpenAPI est un format de contrat qui modélise le comportement d'une API et en facilite l’utilisation par le front-end.

![img](https://docs.wavemaker.com/learn/assets/OpenAPI_Logo.png)

#### Angular

Angular est un FrameWork Front développé par Google, il se base sur la technologie TypeScript développés par Microsoft.
Ce FrameWork permet la réalisation de site web front.
![img](https://www.tic-nova.com/wp-content/uploads/2020/04/angular-10.png)


#### Cordova

Cordova est un moyen de développer une application mobile comme une application web.

![img](https://fishbowlsolutions.com/wp-content/uploads/2020/08/adobe-cordova-300x.png)



#### Clever Cloud

Hébergeur d'applications basé à Nantes, Clever Cloud propose du *Platform As A Service*, aussi appelé *serverless*.
Clever Cloud prend du code et le déploie, c'est aussi simple que ça.

![img](img/clever-cloud.png)

---

### Methodologies

Nous utiliserons Gitlab pour héberger et versionner notre code.
Gitlab met à disposition un outil Kanban qui permet de lister :

- les tâches à faire, et les tâches dont elles dépendent.
- les tâches en cours
- les tâches finies

![img](https://docs.gitlab.com/ee/user/project/img/issue_boards_premium_v14_1.png)

---

### Découpage

- Rédiger le cahier des charges
- Réaliser le diagramme des cas d’utilisations
- Réaliser le diagramme de classe
- Maquetter l'application sur figma
- developper le back-end
- developper le frontend
- déployer l’application
- Tester l’application

![img](gantt/gantt_diagramm.png)

---

### Priorité MVP, *Minimum Viable Product*

L'objectif est de créer un produit minimum viable (MVP), dans l'optique d'itérer à l'avenir
avec de nouvelles fonctionalités, par exemple :

- créer des notifications pour alerter l'utilisateur
- pouvoir utiliser ses anciens parcours pré-enregistrés
- connexion via Facebook, Google…
- intégrer des systèmes de paiement (apple pay, paypal…)
- intégrer les covoiturages dans le calendrier android

---

### Coût

- 8 vendredis
- à 7 heures par vendredi
- x 2 car travail à la maison
- à 2 développeurs
- à 70 € de l’heure

=> 15680 €

### Business Model

L'application sera proposée gratuitement aux étudiants qui l'utiliseront.
Les conducteurs sont rémunérés directement par les passagers.

---

## 3 Clauses légales

Luc Véhi, designé Client, s'engage à verser la somme à la réception du livrable.
Le client s'engage également à n'apporter aucune modification au livrable pendant le développement.
Si toutefois une modification devrais être émise, elle sera discutée et sera facturée à part de la présente.

Alexis Ducoulombier et Emmanuel Bosquet, designés prestaires, s'engagent à réaliser le projet dans son ensemble et à livrer un produit fonctionel dans les délais.
